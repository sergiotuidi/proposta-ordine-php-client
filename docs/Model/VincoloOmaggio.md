# # VincoloOmaggio

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acquistati** | **int** |  |
**omaggio** | **int** |  | [optional] [default to 1]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
