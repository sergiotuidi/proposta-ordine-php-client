# # GruppoArticolo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descrizione** | **string** |  |
**articoli_id** | **string[]** |  | [optional]
**fornitore_id** | **int** |  |
**id** | **int** |  |
**articoli** | [**\PropostaOrdine\Client\Model\Articolo[]**](Articolo.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
