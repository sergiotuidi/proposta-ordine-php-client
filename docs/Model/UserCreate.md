# # UserCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** |  |
**is_active** | **bool** |  | [optional] [default to true]
**is_superuser** | **bool** |  | [optional] [default to false]
**full_name** | **string** |  | [optional]
**password** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
