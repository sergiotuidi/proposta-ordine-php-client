# # BodyUpdateUserMeApiV1UsersMePut

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | **string** |  | [optional]
**full_name** | **string** |  | [optional]
**email** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
