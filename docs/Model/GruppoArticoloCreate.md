# # GruppoArticoloCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descrizione** | **string** |  |
**articoli_id** | **string[]** |  | [optional]
**fornitore_id** | **int** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
