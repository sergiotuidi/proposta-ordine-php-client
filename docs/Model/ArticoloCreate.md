# # ArticoloCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nome** | **string** |  |
**prezzo_vendita** | **float** |  |
**fornitori** | [**\PropostaOrdine\Client\Model\ArticoloFornitore[]**](ArticoloFornitore.md) |  |
**id** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
