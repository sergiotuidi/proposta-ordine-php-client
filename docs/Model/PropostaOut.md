# # PropostaOut

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**articolo_id** | **string** |  |
**qta_pagata** | **int** |  | [optional]
**qta_omaggio** | **int** |  | [optional]
**qta_da_ordinare** | **int** |  | [optional]
**strati** | **int** |  | [optional]
**fabbisogno_soddisfatto** | **float** |  | [optional]
**costo_consegna** | **float** |  | [optional]
**solver_status** | **string** |  | [optional]
**obiettivo** | **float** |  | [optional]
**vincoli** | [**\PropostaOrdine\Client\Model\Vincolo[]**](Vincolo.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
