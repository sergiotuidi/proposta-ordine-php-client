# # ArticoloUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nome** | **string** |  | [optional]
**prezzo_vendita** | **float** |  | [optional]
**fornitori** | [**\PropostaOrdine\Client\Model\ArticoloFornitore[]**](ArticoloFornitore.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
