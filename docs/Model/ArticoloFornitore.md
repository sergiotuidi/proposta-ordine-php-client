# # ArticoloFornitore

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fornitore_id** | **int** |  |
**costo** | **float** |  |
**is_active** | **bool** |  | [optional] [default to true]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
