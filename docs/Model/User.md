# # User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** |  | [optional]
**is_active** | **bool** |  | [optional] [default to true]
**is_superuser** | **bool** |  | [optional] [default to false]
**full_name** | **string** |  | [optional]
**id** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
