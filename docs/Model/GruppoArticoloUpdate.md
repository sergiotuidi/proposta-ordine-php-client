# # GruppoArticoloUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descrizione** | **string** |  | [optional]
**articoli_id** | **string[]** |  | [optional]
**fornitore_id** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
