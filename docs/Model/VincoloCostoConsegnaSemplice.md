# # VincoloCostoConsegnaSemplice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**costi** | [**\PropostaOrdine\Client\Model\CostoConsegnaInterval[]**](CostoConsegnaInterval.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
