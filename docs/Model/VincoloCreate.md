# # VincoloCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_active** | **bool** |  | [optional] [default to true]
**descrizione** | **string** |  | [optional]
**strato** | [**\PropostaOrdine\Client\Model\VincoloStrato**](VincoloStrato.md) |  | [optional]
**omaggio** | [**\PropostaOrdine\Client\Model\VincoloOmaggio**](VincoloOmaggio.md) |  | [optional]
**costo_consegna_semplice** | [**\PropostaOrdine\Client\Model\VincoloCostoConsegnaSemplice**](VincoloCostoConsegnaSemplice.md) |  | [optional]
**tipo** | [**\PropostaOrdine\Client\Model\VincoloType**](VincoloType.md) |  | [optional]
**articolo_id** | **string** |  | [optional]
**fornitore_id** | **int** |  | [optional]
**gruppo_articolo_id** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
