# # PropostaIn

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**articolo_id** | **string** |  |
**domanda** | **float** |  |
**giacenza** | **float** |  |
**service_level** | **float** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
