# PropostaOrdine\Client\PropostaApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**calcolaPropostaApiV1PropostaCalcolaPost()**](PropostaApi.md#calcolaPropostaApiV1PropostaCalcolaPost) | **POST** /api/v1/proposta/calcola | Calcola Proposta


## `calcolaPropostaApiV1PropostaCalcolaPost()`

```php
calcolaPropostaApiV1PropostaCalcolaPost($proposta_in): \PropostaOrdine\Client\Model\PropostaOut[]
```

Calcola Proposta

Calcola proposta.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\PropostaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$proposta_in = array(new \PropostaOrdine\Client\Model\PropostaIn()); // \PropostaOrdine\Client\Model\PropostaIn[]

try {
    $result = $apiInstance->calcolaPropostaApiV1PropostaCalcolaPost($proposta_in);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropostaApi->calcolaPropostaApiV1PropostaCalcolaPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **proposta_in** | [**\PropostaOrdine\Client\Model\PropostaIn[]**](../Model/PropostaIn.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\PropostaOut[]**](../Model/PropostaOut.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
