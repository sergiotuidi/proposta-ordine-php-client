# PropostaOrdine\Client\VincoliApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVincoloApiV1VincoliPost()**](VincoliApi.md#createVincoloApiV1VincoliPost) | **POST** /api/v1/vincoli/ | Create Vincolo
[**deleteVincoloApiV1VincoliIdDelete()**](VincoliApi.md#deleteVincoloApiV1VincoliIdDelete) | **DELETE** /api/v1/vincoli/{id} | Delete Vincolo
[**readVincoliApiV1VincoliGet()**](VincoliApi.md#readVincoliApiV1VincoliGet) | **GET** /api/v1/vincoli/ | Read Vincoli
[**readVincoloApiV1VincoliIdGet()**](VincoliApi.md#readVincoloApiV1VincoliIdGet) | **GET** /api/v1/vincoli/{id} | Read Vincolo
[**updateVincoloApiV1VincoliIdPut()**](VincoliApi.md#updateVincoloApiV1VincoliIdPut) | **PUT** /api/v1/vincoli/{id} | Update Vincolo


## `createVincoloApiV1VincoliPost()`

```php
createVincoloApiV1VincoliPost($vincolo_create): \PropostaOrdine\Client\Model\Vincolo
```

Create Vincolo

Create new vincolo.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\VincoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$vincolo_create = new \PropostaOrdine\Client\Model\VincoloCreate(); // \PropostaOrdine\Client\Model\VincoloCreate

try {
    $result = $apiInstance->createVincoloApiV1VincoliPost($vincolo_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VincoliApi->createVincoloApiV1VincoliPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vincolo_create** | [**\PropostaOrdine\Client\Model\VincoloCreate**](../Model/VincoloCreate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\Vincolo**](../Model/Vincolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteVincoloApiV1VincoliIdDelete()`

```php
deleteVincoloApiV1VincoliIdDelete($id): \PropostaOrdine\Client\Model\Vincolo
```

Delete Vincolo

Delete a vincolo.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\VincoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->deleteVincoloApiV1VincoliIdDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VincoliApi->deleteVincoloApiV1VincoliIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\PropostaOrdine\Client\Model\Vincolo**](../Model/Vincolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readVincoliApiV1VincoliGet()`

```php
readVincoliApiV1VincoliGet($skip, $limit): \PropostaOrdine\Client\Model\Vincolo[]
```

Read Vincoli

Retrieve vincoli.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\VincoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$skip = 0; // int
$limit = 100; // int

try {
    $result = $apiInstance->readVincoliApiV1VincoliGet($skip, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VincoliApi->readVincoliApiV1VincoliGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skip** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]

### Return type

[**\PropostaOrdine\Client\Model\Vincolo[]**](../Model/Vincolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readVincoloApiV1VincoliIdGet()`

```php
readVincoloApiV1VincoliIdGet($id): \PropostaOrdine\Client\Model\Vincolo
```

Read Vincolo

Get vincolo by ID.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\VincoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->readVincoloApiV1VincoliIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VincoliApi->readVincoloApiV1VincoliIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\PropostaOrdine\Client\Model\Vincolo**](../Model/Vincolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateVincoloApiV1VincoliIdPut()`

```php
updateVincoloApiV1VincoliIdPut($id, $vincolo_update): \PropostaOrdine\Client\Model\Vincolo
```

Update Vincolo

Update a vincolo.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\VincoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int
$vincolo_update = new \PropostaOrdine\Client\Model\VincoloUpdate(); // \PropostaOrdine\Client\Model\VincoloUpdate

try {
    $result = $apiInstance->updateVincoloApiV1VincoliIdPut($id, $vincolo_update);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VincoliApi->updateVincoloApiV1VincoliIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **vincolo_update** | [**\PropostaOrdine\Client\Model\VincoloUpdate**](../Model/VincoloUpdate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\Vincolo**](../Model/Vincolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
