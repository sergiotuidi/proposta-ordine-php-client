# PropostaOrdine\Client\GruppiArticoliApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createGruppoArticoliApiV1GruppiArticoliPost()**](GruppiArticoliApi.md#createGruppoArticoliApiV1GruppiArticoliPost) | **POST** /api/v1/gruppi-articoli/ | Create Gruppo Articoli
[**deleteGruppoArticoliApiV1GruppiArticoliIdDelete()**](GruppiArticoliApi.md#deleteGruppoArticoliApiV1GruppiArticoliIdDelete) | **DELETE** /api/v1/gruppi-articoli/{id} | Delete Gruppo Articoli
[**readGruppiArticoliApiV1GruppiArticoliGet()**](GruppiArticoliApi.md#readGruppiArticoliApiV1GruppiArticoliGet) | **GET** /api/v1/gruppi-articoli/ | Read Gruppi Articoli
[**readGruppoArticoliApiV1GruppiArticoliIdGet()**](GruppiArticoliApi.md#readGruppoArticoliApiV1GruppiArticoliIdGet) | **GET** /api/v1/gruppi-articoli/{id} | Read Gruppo Articoli
[**readVincoliByGruppoApiV1GruppiArticoliIdVincoliGet()**](GruppiArticoliApi.md#readVincoliByGruppoApiV1GruppiArticoliIdVincoliGet) | **GET** /api/v1/gruppi-articoli/{id}/vincoli | Read Vincoli By Gruppo
[**updateGruppoArticoliApiV1GruppiArticoliIdPut()**](GruppiArticoliApi.md#updateGruppoArticoliApiV1GruppiArticoliIdPut) | **PUT** /api/v1/gruppi-articoli/{id} | Update Gruppo Articoli


## `createGruppoArticoliApiV1GruppiArticoliPost()`

```php
createGruppoArticoliApiV1GruppiArticoliPost($gruppo_articolo_create): \PropostaOrdine\Client\Model\GruppoArticolo
```

Create Gruppo Articoli

Create new gruppo articoli.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\GruppiArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$gruppo_articolo_create = new \PropostaOrdine\Client\Model\GruppoArticoloCreate(); // \PropostaOrdine\Client\Model\GruppoArticoloCreate

try {
    $result = $apiInstance->createGruppoArticoliApiV1GruppiArticoliPost($gruppo_articolo_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GruppiArticoliApi->createGruppoArticoliApiV1GruppiArticoliPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gruppo_articolo_create** | [**\PropostaOrdine\Client\Model\GruppoArticoloCreate**](../Model/GruppoArticoloCreate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\GruppoArticolo**](../Model/GruppoArticolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteGruppoArticoliApiV1GruppiArticoliIdDelete()`

```php
deleteGruppoArticoliApiV1GruppiArticoliIdDelete($id): \PropostaOrdine\Client\Model\GruppoArticolo
```

Delete Gruppo Articoli

Delete a gruppo articoli.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\GruppiArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->deleteGruppoArticoliApiV1GruppiArticoliIdDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GruppiArticoliApi->deleteGruppoArticoliApiV1GruppiArticoliIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\PropostaOrdine\Client\Model\GruppoArticolo**](../Model/GruppoArticolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readGruppiArticoliApiV1GruppiArticoliGet()`

```php
readGruppiArticoliApiV1GruppiArticoliGet($skip, $limit): \PropostaOrdine\Client\Model\GruppoArticolo[]
```

Read Gruppi Articoli

Retrieve gruppi articoli.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\GruppiArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$skip = 0; // int
$limit = 100; // int

try {
    $result = $apiInstance->readGruppiArticoliApiV1GruppiArticoliGet($skip, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GruppiArticoliApi->readGruppiArticoliApiV1GruppiArticoliGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skip** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]

### Return type

[**\PropostaOrdine\Client\Model\GruppoArticolo[]**](../Model/GruppoArticolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readGruppoArticoliApiV1GruppiArticoliIdGet()`

```php
readGruppoArticoliApiV1GruppiArticoliIdGet($id): \PropostaOrdine\Client\Model\GruppoArticolo
```

Read Gruppo Articoli

Get gruppo articoli by ID.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\GruppiArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->readGruppoArticoliApiV1GruppiArticoliIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GruppiArticoliApi->readGruppoArticoliApiV1GruppiArticoliIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\PropostaOrdine\Client\Model\GruppoArticolo**](../Model/GruppoArticolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readVincoliByGruppoApiV1GruppiArticoliIdVincoliGet()`

```php
readVincoliByGruppoApiV1GruppiArticoliIdVincoliGet($id, $include): \PropostaOrdine\Client\Model\Vincolo[]
```

Read Vincoli By Gruppo

Get vincoli.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\GruppiArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int
$include = new \PropostaOrdine\Client\Model\IncludeVincoli(); // IncludeVincoli

try {
    $result = $apiInstance->readVincoliByGruppoApiV1GruppiArticoliIdVincoliGet($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GruppiArticoliApi->readVincoliByGruppoApiV1GruppiArticoliIdVincoliGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **include** | [**IncludeVincoli**](../Model/.md)|  | [optional]

### Return type

[**\PropostaOrdine\Client\Model\Vincolo[]**](../Model/Vincolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateGruppoArticoliApiV1GruppiArticoliIdPut()`

```php
updateGruppoArticoliApiV1GruppiArticoliIdPut($id, $gruppo_articolo_update): \PropostaOrdine\Client\Model\GruppoArticolo
```

Update Gruppo Articoli

Update a gruppo articoli.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\GruppiArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int
$gruppo_articolo_update = new \PropostaOrdine\Client\Model\GruppoArticoloUpdate(); // \PropostaOrdine\Client\Model\GruppoArticoloUpdate

try {
    $result = $apiInstance->updateGruppoArticoliApiV1GruppiArticoliIdPut($id, $gruppo_articolo_update);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GruppiArticoliApi->updateGruppoArticoliApiV1GruppiArticoliIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **gruppo_articolo_update** | [**\PropostaOrdine\Client\Model\GruppoArticoloUpdate**](../Model/GruppoArticoloUpdate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\GruppoArticolo**](../Model/GruppoArticolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
