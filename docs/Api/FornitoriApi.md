# PropostaOrdine\Client\FornitoriApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createFornitoreApiV1FornitoriPost()**](FornitoriApi.md#createFornitoreApiV1FornitoriPost) | **POST** /api/v1/fornitori/ | Create Fornitore
[**deleteFornitoreApiV1FornitoriIdDelete()**](FornitoriApi.md#deleteFornitoreApiV1FornitoriIdDelete) | **DELETE** /api/v1/fornitori/{id} | Delete Fornitore
[**readFornitoreApiV1FornitoriIdGet()**](FornitoriApi.md#readFornitoreApiV1FornitoriIdGet) | **GET** /api/v1/fornitori/{id} | Read Fornitore
[**readFornitoriApiV1FornitoriGet()**](FornitoriApi.md#readFornitoriApiV1FornitoriGet) | **GET** /api/v1/fornitori/ | Read Fornitori
[**readVincoliByFornitoreApiV1FornitoriIdVincoliGet()**](FornitoriApi.md#readVincoliByFornitoreApiV1FornitoriIdVincoliGet) | **GET** /api/v1/fornitori/{id}/vincoli | Read Vincoli By Fornitore
[**updateFornitoreApiV1FornitoriIdPut()**](FornitoriApi.md#updateFornitoreApiV1FornitoriIdPut) | **PUT** /api/v1/fornitori/{id} | Update Fornitore


## `createFornitoreApiV1FornitoriPost()`

```php
createFornitoreApiV1FornitoriPost($fornitore_create): \PropostaOrdine\Client\Model\Fornitore
```

Create Fornitore

Create new fornitore.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\FornitoriApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$fornitore_create = new \PropostaOrdine\Client\Model\FornitoreCreate(); // \PropostaOrdine\Client\Model\FornitoreCreate

try {
    $result = $apiInstance->createFornitoreApiV1FornitoriPost($fornitore_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FornitoriApi->createFornitoreApiV1FornitoriPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fornitore_create** | [**\PropostaOrdine\Client\Model\FornitoreCreate**](../Model/FornitoreCreate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\Fornitore**](../Model/Fornitore.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteFornitoreApiV1FornitoriIdDelete()`

```php
deleteFornitoreApiV1FornitoriIdDelete($id): \PropostaOrdine\Client\Model\Fornitore
```

Delete Fornitore

Delete a fornitore.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\FornitoriApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->deleteFornitoreApiV1FornitoriIdDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FornitoriApi->deleteFornitoreApiV1FornitoriIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\PropostaOrdine\Client\Model\Fornitore**](../Model/Fornitore.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readFornitoreApiV1FornitoriIdGet()`

```php
readFornitoreApiV1FornitoriIdGet($id): \PropostaOrdine\Client\Model\Fornitore
```

Read Fornitore

Get fornitore by ID.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\FornitoriApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->readFornitoreApiV1FornitoriIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FornitoriApi->readFornitoreApiV1FornitoriIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\PropostaOrdine\Client\Model\Fornitore**](../Model/Fornitore.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readFornitoriApiV1FornitoriGet()`

```php
readFornitoriApiV1FornitoriGet($skip, $limit): \PropostaOrdine\Client\Model\Fornitore[]
```

Read Fornitori

Retrieve fornitori.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\FornitoriApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$skip = 0; // int
$limit = 100; // int

try {
    $result = $apiInstance->readFornitoriApiV1FornitoriGet($skip, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FornitoriApi->readFornitoriApiV1FornitoriGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skip** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]

### Return type

[**\PropostaOrdine\Client\Model\Fornitore[]**](../Model/Fornitore.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readVincoliByFornitoreApiV1FornitoriIdVincoliGet()`

```php
readVincoliByFornitoreApiV1FornitoriIdVincoliGet($id): \PropostaOrdine\Client\Model\Vincolo[]
```

Read Vincoli By Fornitore

Get vincoli.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\FornitoriApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->readVincoliByFornitoreApiV1FornitoriIdVincoliGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FornitoriApi->readVincoliByFornitoreApiV1FornitoriIdVincoliGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\PropostaOrdine\Client\Model\Vincolo[]**](../Model/Vincolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateFornitoreApiV1FornitoriIdPut()`

```php
updateFornitoreApiV1FornitoriIdPut($id, $fornitore_update): \PropostaOrdine\Client\Model\Fornitore
```

Update Fornitore

Update a fornitore.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\FornitoriApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int
$fornitore_update = new \PropostaOrdine\Client\Model\FornitoreUpdate(); // \PropostaOrdine\Client\Model\FornitoreUpdate

try {
    $result = $apiInstance->updateFornitoreApiV1FornitoriIdPut($id, $fornitore_update);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FornitoriApi->updateFornitoreApiV1FornitoriIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **fornitore_update** | [**\PropostaOrdine\Client\Model\FornitoreUpdate**](../Model/FornitoreUpdate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\Fornitore**](../Model/Fornitore.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
