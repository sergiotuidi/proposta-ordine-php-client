# PropostaOrdine\Client\ArticoliApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createArticoloApiV1ArticoliPost()**](ArticoliApi.md#createArticoloApiV1ArticoliPost) | **POST** /api/v1/articoli/ | Create Articolo
[**deleteArticoloApiV1ArticoliIdDelete()**](ArticoliApi.md#deleteArticoloApiV1ArticoliIdDelete) | **DELETE** /api/v1/articoli/{id} | Delete Articolo
[**readArticoliApiV1ArticoliGet()**](ArticoliApi.md#readArticoliApiV1ArticoliGet) | **GET** /api/v1/articoli/ | Read Articoli
[**readArticoloApiV1ArticoliIdGet()**](ArticoliApi.md#readArticoloApiV1ArticoliIdGet) | **GET** /api/v1/articoli/{id} | Read Articolo
[**readVincoliByArticoloApiV1ArticoliIdVincoliGet()**](ArticoliApi.md#readVincoliByArticoloApiV1ArticoliIdVincoliGet) | **GET** /api/v1/articoli/{id}/vincoli | Read Vincoli By Articolo
[**updateArticoloApiV1ArticoliIdPut()**](ArticoliApi.md#updateArticoloApiV1ArticoliIdPut) | **PUT** /api/v1/articoli/{id} | Update Articolo


## `createArticoloApiV1ArticoliPost()`

```php
createArticoloApiV1ArticoliPost($articolo_create): \PropostaOrdine\Client\Model\Articolo
```

Create Articolo

Create new articolo.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\ArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$articolo_create = new \PropostaOrdine\Client\Model\ArticoloCreate(); // \PropostaOrdine\Client\Model\ArticoloCreate

try {
    $result = $apiInstance->createArticoloApiV1ArticoliPost($articolo_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticoliApi->createArticoloApiV1ArticoliPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **articolo_create** | [**\PropostaOrdine\Client\Model\ArticoloCreate**](../Model/ArticoloCreate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\Articolo**](../Model/Articolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteArticoloApiV1ArticoliIdDelete()`

```php
deleteArticoloApiV1ArticoliIdDelete($id): \PropostaOrdine\Client\Model\Articolo
```

Delete Articolo

Delete a articolo.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\ArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string

try {
    $result = $apiInstance->deleteArticoloApiV1ArticoliIdDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticoliApi->deleteArticoloApiV1ArticoliIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\PropostaOrdine\Client\Model\Articolo**](../Model/Articolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readArticoliApiV1ArticoliGet()`

```php
readArticoliApiV1ArticoliGet($skip, $limit): \PropostaOrdine\Client\Model\Articolo[]
```

Read Articoli

Retrieve articoli.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\ArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$skip = 0; // int
$limit = 100; // int

try {
    $result = $apiInstance->readArticoliApiV1ArticoliGet($skip, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticoliApi->readArticoliApiV1ArticoliGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skip** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]

### Return type

[**\PropostaOrdine\Client\Model\Articolo[]**](../Model/Articolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readArticoloApiV1ArticoliIdGet()`

```php
readArticoloApiV1ArticoliIdGet($id): \PropostaOrdine\Client\Model\Articolo
```

Read Articolo

Get articolo by ID.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\ArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string

try {
    $result = $apiInstance->readArticoloApiV1ArticoliIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticoliApi->readArticoloApiV1ArticoliIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\PropostaOrdine\Client\Model\Articolo**](../Model/Articolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readVincoliByArticoloApiV1ArticoliIdVincoliGet()`

```php
readVincoliByArticoloApiV1ArticoliIdVincoliGet($id, $include): \PropostaOrdine\Client\Model\Vincolo[]
```

Read Vincoli By Articolo

Get vincoli.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\ArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string
$include = new \PropostaOrdine\Client\Model\IncludeVincoli(); // IncludeVincoli

try {
    $result = $apiInstance->readVincoliByArticoloApiV1ArticoliIdVincoliGet($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticoliApi->readVincoliByArticoloApiV1ArticoliIdVincoliGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **include** | [**IncludeVincoli**](../Model/.md)|  | [optional]

### Return type

[**\PropostaOrdine\Client\Model\Vincolo[]**](../Model/Vincolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateArticoloApiV1ArticoliIdPut()`

```php
updateArticoloApiV1ArticoliIdPut($id, $articolo_update): \PropostaOrdine\Client\Model\Articolo
```

Update Articolo

Update a articolo.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\ArticoliApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string
$articolo_update = new \PropostaOrdine\Client\Model\ArticoloUpdate(); // \PropostaOrdine\Client\Model\ArticoloUpdate

try {
    $result = $apiInstance->updateArticoloApiV1ArticoliIdPut($id, $articolo_update);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticoliApi->updateArticoloApiV1ArticoliIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **articolo_update** | [**\PropostaOrdine\Client\Model\ArticoloUpdate**](../Model/ArticoloUpdate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\Articolo**](../Model/Articolo.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
