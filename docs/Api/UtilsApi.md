# PropostaOrdine\Client\UtilsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**testCeleryApiV1UtilsTestCeleryPost()**](UtilsApi.md#testCeleryApiV1UtilsTestCeleryPost) | **POST** /api/v1/utils/test-celery/ | Test Celery
[**testEmailApiV1UtilsTestEmailPost()**](UtilsApi.md#testEmailApiV1UtilsTestEmailPost) | **POST** /api/v1/utils/test-email/ | Test Email


## `testCeleryApiV1UtilsTestCeleryPost()`

```php
testCeleryApiV1UtilsTestCeleryPost($msg): \PropostaOrdine\Client\Model\Msg
```

Test Celery

Test Celery worker.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\UtilsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$msg = new \PropostaOrdine\Client\Model\Msg(); // \PropostaOrdine\Client\Model\Msg

try {
    $result = $apiInstance->testCeleryApiV1UtilsTestCeleryPost($msg);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UtilsApi->testCeleryApiV1UtilsTestCeleryPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **msg** | [**\PropostaOrdine\Client\Model\Msg**](../Model/Msg.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\Msg**](../Model/Msg.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `testEmailApiV1UtilsTestEmailPost()`

```php
testEmailApiV1UtilsTestEmailPost($email_to): \PropostaOrdine\Client\Model\Msg
```

Test Email

Test emails.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\UtilsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$email_to = 'email_to_example'; // string

try {
    $result = $apiInstance->testEmailApiV1UtilsTestEmailPost($email_to);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UtilsApi->testEmailApiV1UtilsTestEmailPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email_to** | [**string**](../Model/.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\Msg**](../Model/Msg.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
