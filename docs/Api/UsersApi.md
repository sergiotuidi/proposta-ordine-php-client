# PropostaOrdine\Client\UsersApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUserApiV1UsersPost()**](UsersApi.md#createUserApiV1UsersPost) | **POST** /api/v1/users/ | Create User
[**createUserOpenApiV1UsersOpenPost()**](UsersApi.md#createUserOpenApiV1UsersOpenPost) | **POST** /api/v1/users/open | Create User Open
[**readUserByIdApiV1UsersUserIdGet()**](UsersApi.md#readUserByIdApiV1UsersUserIdGet) | **GET** /api/v1/users/{user_id} | Read User By Id
[**readUserMeApiV1UsersMeGet()**](UsersApi.md#readUserMeApiV1UsersMeGet) | **GET** /api/v1/users/me | Read User Me
[**readUsersApiV1UsersGet()**](UsersApi.md#readUsersApiV1UsersGet) | **GET** /api/v1/users/ | Read Users
[**updateUserApiV1UsersUserIdPut()**](UsersApi.md#updateUserApiV1UsersUserIdPut) | **PUT** /api/v1/users/{user_id} | Update User
[**updateUserMeApiV1UsersMePut()**](UsersApi.md#updateUserMeApiV1UsersMePut) | **PUT** /api/v1/users/me | Update User Me


## `createUserApiV1UsersPost()`

```php
createUserApiV1UsersPost($user_create): \PropostaOrdine\Client\Model\User
```

Create User

Create new user.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user_create = new \PropostaOrdine\Client\Model\UserCreate(); // \PropostaOrdine\Client\Model\UserCreate

try {
    $result = $apiInstance->createUserApiV1UsersPost($user_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->createUserApiV1UsersPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_create** | [**\PropostaOrdine\Client\Model\UserCreate**](../Model/UserCreate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\User**](../Model/User.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `createUserOpenApiV1UsersOpenPost()`

```php
createUserOpenApiV1UsersOpenPost($body_create_user_open_api_v1_users_open_post): \PropostaOrdine\Client\Model\User
```

Create User Open

Create new user without the need to be logged in.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new PropostaOrdine\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body_create_user_open_api_v1_users_open_post = new \PropostaOrdine\Client\Model\BodyCreateUserOpenApiV1UsersOpenPost(); // \PropostaOrdine\Client\Model\BodyCreateUserOpenApiV1UsersOpenPost

try {
    $result = $apiInstance->createUserOpenApiV1UsersOpenPost($body_create_user_open_api_v1_users_open_post);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->createUserOpenApiV1UsersOpenPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body_create_user_open_api_v1_users_open_post** | [**\PropostaOrdine\Client\Model\BodyCreateUserOpenApiV1UsersOpenPost**](../Model/BodyCreateUserOpenApiV1UsersOpenPost.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\User**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readUserByIdApiV1UsersUserIdGet()`

```php
readUserByIdApiV1UsersUserIdGet($user_id): \PropostaOrdine\Client\Model\User
```

Read User By Id

Get a specific user by id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user_id = 56; // int

try {
    $result = $apiInstance->readUserByIdApiV1UsersUserIdGet($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->readUserByIdApiV1UsersUserIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**|  |

### Return type

[**\PropostaOrdine\Client\Model\User**](../Model/User.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readUserMeApiV1UsersMeGet()`

```php
readUserMeApiV1UsersMeGet(): \PropostaOrdine\Client\Model\User
```

Read User Me

Get current user.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->readUserMeApiV1UsersMeGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->readUserMeApiV1UsersMeGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\PropostaOrdine\Client\Model\User**](../Model/User.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readUsersApiV1UsersGet()`

```php
readUsersApiV1UsersGet($skip, $limit): \PropostaOrdine\Client\Model\User[]
```

Read Users

Retrieve users.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$skip = 0; // int
$limit = 100; // int

try {
    $result = $apiInstance->readUsersApiV1UsersGet($skip, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->readUsersApiV1UsersGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skip** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]

### Return type

[**\PropostaOrdine\Client\Model\User[]**](../Model/User.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateUserApiV1UsersUserIdPut()`

```php
updateUserApiV1UsersUserIdPut($user_id, $user_update): \PropostaOrdine\Client\Model\User
```

Update User

Update a user.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user_id = 56; // int
$user_update = new \PropostaOrdine\Client\Model\UserUpdate(); // \PropostaOrdine\Client\Model\UserUpdate

try {
    $result = $apiInstance->updateUserApiV1UsersUserIdPut($user_id, $user_update);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->updateUserApiV1UsersUserIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**|  |
 **user_update** | [**\PropostaOrdine\Client\Model\UserUpdate**](../Model/UserUpdate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\User**](../Model/User.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateUserMeApiV1UsersMePut()`

```php
updateUserMeApiV1UsersMePut($body_update_user_me_api_v1_users_me_put): \PropostaOrdine\Client\Model\User
```

Update User Me

Update own user.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body_update_user_me_api_v1_users_me_put = new \PropostaOrdine\Client\Model\BodyUpdateUserMeApiV1UsersMePut(); // \PropostaOrdine\Client\Model\BodyUpdateUserMeApiV1UsersMePut

try {
    $result = $apiInstance->updateUserMeApiV1UsersMePut($body_update_user_me_api_v1_users_me_put);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->updateUserMeApiV1UsersMePut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body_update_user_me_api_v1_users_me_put** | [**\PropostaOrdine\Client\Model\BodyUpdateUserMeApiV1UsersMePut**](../Model/BodyUpdateUserMeApiV1UsersMePut.md)|  | [optional]

### Return type

[**\PropostaOrdine\Client\Model\User**](../Model/User.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
