# PropostaOrdine\Client\LoginApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**loginAccessTokenApiV1LoginAccessTokenPost()**](LoginApi.md#loginAccessTokenApiV1LoginAccessTokenPost) | **POST** /api/v1/login/access-token | Login Access Token
[**recoverPasswordApiV1PasswordRecoveryEmailPost()**](LoginApi.md#recoverPasswordApiV1PasswordRecoveryEmailPost) | **POST** /api/v1/password-recovery/{email} | Recover Password
[**resetPasswordApiV1ResetPasswordPost()**](LoginApi.md#resetPasswordApiV1ResetPasswordPost) | **POST** /api/v1/reset-password/ | Reset Password
[**testTokenApiV1LoginTestTokenPost()**](LoginApi.md#testTokenApiV1LoginTestTokenPost) | **POST** /api/v1/login/test-token | Test Token


## `loginAccessTokenApiV1LoginAccessTokenPost()`

```php
loginAccessTokenApiV1LoginAccessTokenPost($username, $password, $grant_type, $scope, $client_id, $client_secret): \PropostaOrdine\Client\Model\Token
```

Login Access Token

OAuth2 compatible token login, get an access token for future requests

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new PropostaOrdine\Client\Api\LoginApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$username = 'username_example'; // string
$password = 'password_example'; // string
$grant_type = 'grant_type_example'; // string
$scope = ''; // string
$client_id = 'client_id_example'; // string
$client_secret = 'client_secret_example'; // string

try {
    $result = $apiInstance->loginAccessTokenApiV1LoginAccessTokenPost($username, $password, $grant_type, $scope, $client_id, $client_secret);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LoginApi->loginAccessTokenApiV1LoginAccessTokenPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  |
 **password** | **string**|  |
 **grant_type** | **string**|  | [optional]
 **scope** | **string**|  | [optional] [default to &#39;&#39;]
 **client_id** | **string**|  | [optional]
 **client_secret** | **string**|  | [optional]

### Return type

[**\PropostaOrdine\Client\Model\Token**](../Model/Token.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recoverPasswordApiV1PasswordRecoveryEmailPost()`

```php
recoverPasswordApiV1PasswordRecoveryEmailPost($email): \PropostaOrdine\Client\Model\Msg
```

Recover Password

Password Recovery

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new PropostaOrdine\Client\Api\LoginApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$email = 'email_example'; // string

try {
    $result = $apiInstance->recoverPasswordApiV1PasswordRecoveryEmailPost($email);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LoginApi->recoverPasswordApiV1PasswordRecoveryEmailPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **string**|  |

### Return type

[**\PropostaOrdine\Client\Model\Msg**](../Model/Msg.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `resetPasswordApiV1ResetPasswordPost()`

```php
resetPasswordApiV1ResetPasswordPost($body_reset_password_api_v1_reset_password_post): \PropostaOrdine\Client\Model\Msg
```

Reset Password

Reset password

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new PropostaOrdine\Client\Api\LoginApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body_reset_password_api_v1_reset_password_post = new \PropostaOrdine\Client\Model\BodyResetPasswordApiV1ResetPasswordPost(); // \PropostaOrdine\Client\Model\BodyResetPasswordApiV1ResetPasswordPost

try {
    $result = $apiInstance->resetPasswordApiV1ResetPasswordPost($body_reset_password_api_v1_reset_password_post);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LoginApi->resetPasswordApiV1ResetPasswordPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body_reset_password_api_v1_reset_password_post** | [**\PropostaOrdine\Client\Model\BodyResetPasswordApiV1ResetPasswordPost**](../Model/BodyResetPasswordApiV1ResetPasswordPost.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\Msg**](../Model/Msg.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `testTokenApiV1LoginTestTokenPost()`

```php
testTokenApiV1LoginTestTokenPost(): \PropostaOrdine\Client\Model\User
```

Test Token

Test access token

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\LoginApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->testTokenApiV1LoginTestTokenPost();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LoginApi->testTokenApiV1LoginTestTokenPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\PropostaOrdine\Client\Model\User**](../Model/User.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
