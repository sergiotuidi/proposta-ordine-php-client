# PropostaOrdine\Client\ItemsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createItemApiV1ItemsPost()**](ItemsApi.md#createItemApiV1ItemsPost) | **POST** /api/v1/items/ | Create Item
[**deleteItemApiV1ItemsIdDelete()**](ItemsApi.md#deleteItemApiV1ItemsIdDelete) | **DELETE** /api/v1/items/{id} | Delete Item
[**readItemApiV1ItemsIdGet()**](ItemsApi.md#readItemApiV1ItemsIdGet) | **GET** /api/v1/items/{id} | Read Item
[**readItemsApiV1ItemsGet()**](ItemsApi.md#readItemsApiV1ItemsGet) | **GET** /api/v1/items/ | Read Items
[**updateItemApiV1ItemsIdPut()**](ItemsApi.md#updateItemApiV1ItemsIdPut) | **PUT** /api/v1/items/{id} | Update Item


## `createItemApiV1ItemsPost()`

```php
createItemApiV1ItemsPost($item_create): \PropostaOrdine\Client\Model\Item
```

Create Item

Create new item.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$item_create = new \PropostaOrdine\Client\Model\ItemCreate(); // \PropostaOrdine\Client\Model\ItemCreate

try {
    $result = $apiInstance->createItemApiV1ItemsPost($item_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->createItemApiV1ItemsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item_create** | [**\PropostaOrdine\Client\Model\ItemCreate**](../Model/ItemCreate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\Item**](../Model/Item.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteItemApiV1ItemsIdDelete()`

```php
deleteItemApiV1ItemsIdDelete($id): \PropostaOrdine\Client\Model\Item
```

Delete Item

Delete an item.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->deleteItemApiV1ItemsIdDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->deleteItemApiV1ItemsIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\PropostaOrdine\Client\Model\Item**](../Model/Item.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readItemApiV1ItemsIdGet()`

```php
readItemApiV1ItemsIdGet($id): \PropostaOrdine\Client\Model\Item
```

Read Item

Get item by ID.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->readItemApiV1ItemsIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->readItemApiV1ItemsIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\PropostaOrdine\Client\Model\Item**](../Model/Item.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readItemsApiV1ItemsGet()`

```php
readItemsApiV1ItemsGet($skip, $limit): \PropostaOrdine\Client\Model\Item[]
```

Read Items

Retrieve items.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$skip = 0; // int
$limit = 100; // int

try {
    $result = $apiInstance->readItemsApiV1ItemsGet($skip, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->readItemsApiV1ItemsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skip** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]

### Return type

[**\PropostaOrdine\Client\Model\Item[]**](../Model/Item.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateItemApiV1ItemsIdPut()`

```php
updateItemApiV1ItemsIdPut($id, $item_update): \PropostaOrdine\Client\Model\Item
```

Update Item

Update an item.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
$config = PropostaOrdine\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PropostaOrdine\Client\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int
$item_update = new \PropostaOrdine\Client\Model\ItemUpdate(); // \PropostaOrdine\Client\Model\ItemUpdate

try {
    $result = $apiInstance->updateItemApiV1ItemsIdPut($id, $item_update);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->updateItemApiV1ItemsIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **item_update** | [**\PropostaOrdine\Client\Model\ItemUpdate**](../Model/ItemUpdate.md)|  |

### Return type

[**\PropostaOrdine\Client\Model\Item**](../Model/Item.md)

### Authorization

[OAuth2PasswordBearer](../../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
