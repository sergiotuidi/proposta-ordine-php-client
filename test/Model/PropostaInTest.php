<?php
/**
 * PropostaInTest
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  PropostaOrdine\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Proposta Ordine Backend
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace PropostaOrdine\Client\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * PropostaInTest Class Doc Comment
 *
 * @category    Class
 * @description PropostaIn
 * @package     PropostaOrdine\Client
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class PropostaInTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "PropostaIn"
     */
    public function testPropostaIn()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "articolo_id"
     */
    public function testPropertyArticoloId()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "domanda"
     */
    public function testPropertyDomanda()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "giacenza"
     */
    public function testPropertyGiacenza()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "service_level"
     */
    public function testPropertyServiceLevel()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
