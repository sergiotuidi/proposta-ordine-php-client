<?php
/**
 * ArticoliApiTest
 * PHP version 7.2
 *
 * @category Class
 * @package  PropostaOrdine\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Proposta Ordine Backend
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace PropostaOrdine\Client\Test\Api;

use \PropostaOrdine\Client\Configuration;
use \PropostaOrdine\Client\ApiException;
use \PropostaOrdine\Client\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * ArticoliApiTest Class Doc Comment
 *
 * @category Class
 * @package  PropostaOrdine\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class ArticoliApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for createArticoloApiV1ArticoliPost
     *
     * Create Articolo.
     *
     */
    public function testCreateArticoloApiV1ArticoliPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for deleteArticoloApiV1ArticoliIdDelete
     *
     * Delete Articolo.
     *
     */
    public function testDeleteArticoloApiV1ArticoliIdDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for readArticoliApiV1ArticoliGet
     *
     * Read Articoli.
     *
     */
    public function testReadArticoliApiV1ArticoliGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for readArticoloApiV1ArticoliIdGet
     *
     * Read Articolo.
     *
     */
    public function testReadArticoloApiV1ArticoliIdGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for readVincoliByArticoloApiV1ArticoliIdVincoliGet
     *
     * Read Vincoli By Articolo.
     *
     */
    public function testReadVincoliByArticoloApiV1ArticoliIdVincoliGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for updateArticoloApiV1ArticoliIdPut
     *
     * Update Articolo.
     *
     */
    public function testUpdateArticoloApiV1ArticoliIdPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
